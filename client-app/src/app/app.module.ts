import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExchangeratesComponent } from './exchangerates/exchangerates.component';
import { ExchangerateService } from './exchangerates/exchangerate.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//import { FormComponent } from './exchangerates/exchangerates.component';
import { FormsModule } from '@angular/forms';
import { FormComponent } from './exchangerates/form.component'
import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  {
    path: 'formulario',
    component: FormComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ExchangeratesComponent
  ],
  imports: [
    [RouterModule.forRoot(routes)],
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
    
  ],
  providers: [ExchangerateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
