import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Front con angular que consume API Exchange Rate';

  consumo: string = 'Consumo de API Exchange Rate';
}
