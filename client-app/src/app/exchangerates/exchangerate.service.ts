import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExchangeRate } from './exchangerate';
import { ExchangeRateResponse } from './exchangerateresponse';
import { ExchangeRateSave } from './exchangeratesave';
import { ExchangeRateSaveResponse } from './exchangeratesaveresponse';

@Injectable()
export class ExchangerateService {

  private urlEndPoint: string = '/api/v1/exchange-rate/calculate';
  private urlEndPoint2: string = '/api/v1/exchange-rate/save';
  constructor(private http: HttpClient) { }

  getExchangeRates(exchangerate: ExchangeRate): Observable<ExchangeRateResponse> {    
    return this.http.post<ExchangeRateResponse>(this.urlEndPoint, exchangerate);
  }

  saveExchangeRates(exchangeratesave: ExchangeRateSave): Observable<ExchangeRateSaveResponse> {    
    return this.http.post<ExchangeRateSaveResponse>(this.urlEndPoint2, exchangeratesave);
  }

}
