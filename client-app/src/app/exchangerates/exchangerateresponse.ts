export class ExchangeRateResponse {
    amount: number;
    calculatedAmount: number;
    originCurrency: string;
    destinationCurrency: string;
    exchangeRateValue: number;
    date: string
}