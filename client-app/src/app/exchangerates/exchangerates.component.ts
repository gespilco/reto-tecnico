import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ExchangeRate } from './exchangerate';
import { ExchangerateService } from './exchangerate.service';

@Component({
  selector: 'app-exchangerates',
  templateUrl: './exchangerates.component.html'
})
export class ExchangeratesComponent implements OnInit {

  exchangerate: any ={};

  constructor(private exchangeRateService: ExchangerateService) { }

  amount: number=0;
  originCurrency: string='';
  destinationCurrency
  exchangeRateValue: '';
  date: string='';


  exchangerateResponse: any = {
    code: "",
    exchangeRateValue: '',
    originCurrency: "",
    destinationCurrency: "",
    date: "",
  };

  exchangerateSaveResponse: any = {
    amount: '',
    calculatedAmount: '',
    originCurrency: "",
    destinationCurrency: "",
    exchangeRateValue: ''
  };

  ngOnInit() {

  }

  onSubmit() {
    this.calculate();
  }

  calculate() {
    this.exchangeRateService.getExchangeRates(this.exchangerate).subscribe(resp => {
      this.exchangerate = resp; 
    }); 
  }

  calculateExchangeRate(): void {
    this.exchangerate.amount = this.amount;
    this.exchangerate.destinationCurrency = this.destinationCurrency;
    this.exchangerate.originCurrency = this.originCurrency;
    this.exchangerate.date = this.date;
    console.log(this.exchangerate);
    this.exchangeRateService.getExchangeRates(this.exchangerate).subscribe((resp => {
      this.exchangerateResponse = resp;
    }));  
  }
  
  save(): void {
    this.exchangerate.exchangeRateValue = this.exchangerateResponse.exchangeRateValue;
    this.exchangerate.destinationCurrency = this.exchangerateResponse.destinationCurrency;
    this.exchangerate.originCurrency = this.exchangerateResponse.originCurrency;
    console.log(this.exchangerate);
    this.exchangeRateService.saveExchangeRates(this.exchangerate).subscribe(resp => {
      this.exchangerateSaveResponse = resp; 
    }); 
  }

 

}
