import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ExchangeRate } from './exchangerate';
import { ExchangerateService } from './exchangerate.service';

@Component({
  selector: 'app-form',
  templateUrl: 'form.component.html'
})
export class FormComponent implements OnInit {

  exchangerate: any ={};

  constructor(private exchangeRateService: ExchangerateService) { }

  amount: number=0;
  originCurrency: string='';
  destinationCurrency: string='';
  exchangeRateValue: '';

  exchangerateSaveResponse: any = {
    amount: '',
    calculatedAmount: '',
    originCurrency: "",
    destinationCurrency: "",
    exchangeRateValue: ''
  };

  ngOnInit() {
    console.log('flag1')
  }

  save(): void {
    this.exchangerate.exchangeRateValue = this.exchangeRateValue;
    this.exchangerate.destinationCurrency = this.destinationCurrency;
    this.exchangerate.originCurrency = this.originCurrency;
    console.log(this.exchangerate);
    this.exchangeRateService.saveExchangeRates(this.exchangerate).subscribe(resp => {
      this.exchangerateSaveResponse = resp;
    }); 
  }

}
