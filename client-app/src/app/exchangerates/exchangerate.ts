export class ExchangeRate {
    amount: number;
    originCurrency: string;
    destinationCurrency: string
    date: string
}