export class ExchangeRateSave {
    code: string;
    exchangeRateValue: number;
    originCurrency: string;
    destinationCurrency: string
}
