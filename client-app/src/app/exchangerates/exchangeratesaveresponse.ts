export class ExchangeRateSaveResponse {
    code: string;
    exchangeRateValue: number;
    originCurrency: string;
    destinationCurrency: string
}