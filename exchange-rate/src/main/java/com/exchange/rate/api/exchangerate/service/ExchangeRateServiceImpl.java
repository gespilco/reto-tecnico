package com.exchange.rate.api.exchangerate.service;

import ch.qos.logback.classic.Logger;
import com.exchange.rate.api.exchangerate.model.entity.ExchangeRate;
import com.exchange.rate.api.exchangerate.model.request.ExchangeRateRequest;
import com.exchange.rate.api.exchangerate.model.request.ExchangeRateSearchRequest;
import com.exchange.rate.api.exchangerate.model.request.UpdateExchangeRateRequest;
import com.exchange.rate.api.exchangerate.model.response.ExchangeRateResponse;
import com.exchange.rate.api.exchangerate.model.response.ExchangeRateSearchResponse;
import com.exchange.rate.api.exchangerate.repository.ExchangeRateRepository;
import org.apache.tomcat.util.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Completable;
import rx.Single;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

    @Override
    public Single<ExchangeRateSearchResponse> calculate(ExchangeRateSearchRequest request) {
        String code = request.getOriginCurrency().concat(request.getDestinationCurrency()).concat(request.getDate());

        Optional<ExchangeRate> exchangeRate =
                exchangeRateRepository.findById(code);
        if (exchangeRate.isPresent()){
            Optional.ofNullable(exchangeRate.get().getExchangeRateValue())
                    .orElseThrow(() -> new ArithmeticException(
                            "No se encontro el valor de tipo de cambio para la conversion de "
                                    + request.getOriginCurrency() + " a " + request.getDestinationCurrency()
                                    + " con fecha " + request.getDate()));
            ExchangeRateSearchResponse response =  ExchangeRateSearchResponse.builder()
                    .amount(request.getAmount())
                    .calculatedAmount(exchangeRate.get().getExchangeRateValue()
                            .multiply(request.getAmount()))
                    .originCurrency(exchangeRate.get().getOriginCurrency())
                    .destinationCurrency(exchangeRate.get().getDestinationCurrency())
                    .exchangeRateValue(exchangeRate.get().getExchangeRateValue())
                    .build();
            return Single.just(response);
        }else{
            throw new EntityNotFoundException(
                    "No se encontro informacion del tipo de cambio para la conversion de "
                            + request.getOriginCurrency() + " a " + request.getDestinationCurrency() +
                    " con fecha " + request.getDate());
        }
    }

    @Override
    public Single<ExchangeRateResponse> save(ExchangeRateRequest request) {
        ExchangeRate exchangeRate  = ExchangeRate.builder()
                .code(request.getOriginCurrency().concat(request.getDestinationCurrency()))
                .originCurrency(request.getOriginCurrency())
                .destinationCurrency(request.getDestinationCurrency())
                .exchangeRateValue(request.getExchangeRateValue())
                .build();
        exchangeRateRepository.save(exchangeRate);
        return Single.just(ExchangeRateResponse.builder()
                .exchangeRateValue(request.getExchangeRateValue())
                .originCurrency(request.getOriginCurrency())
                .destinationCurrency(request.getDestinationCurrency())
                .code(exchangeRate.getCode())
                .build()
        );
    }

    @Override
    public Completable updateExchangeRate(UpdateExchangeRateRequest exchangeRateRequest) {
        return updateExchangeRateToRepository(exchangeRateRequest);
    }

    private Completable updateExchangeRateToRepository(UpdateExchangeRateRequest exchangeRateRequest) {
        return Completable.create(completableSubscriber -> {
            Optional<ExchangeRate> optionalExchangeRate = exchangeRateRepository
                    .findById(exchangeRateRequest.getCode());
            if (!optionalExchangeRate.isPresent())
                completableSubscriber.onError(new EntityNotFoundException());
            else {
                ExchangeRate exchangeRate = optionalExchangeRate.get();
                exchangeRate.setExchangeRateValue(exchangeRateRequest.getExchangeRateValue());
                exchangeRateRepository.save(exchangeRate);
                completableSubscriber.onCompleted();
            }
        });
    }
}
