package com.exchange.rate.api.exchangerate.model.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class UpdateExchangeRateRequest {
    private String code;
    private BigDecimal exchangeRateValue;
}
