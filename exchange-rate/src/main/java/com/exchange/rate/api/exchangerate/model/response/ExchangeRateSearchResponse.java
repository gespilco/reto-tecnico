package com.exchange.rate.api.exchangerate.model.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
public class ExchangeRateSearchResponse {
    //private String code;
    private BigDecimal amount; //monto base
    private BigDecimal calculatedAmount; // monto calculado
    private String originCurrency; // moneda inicial
    private String destinationCurrency; // moneda destino
    private BigDecimal exchangeRateValue; //tasa/tipo de cambio que se aplicara al monto base
}
