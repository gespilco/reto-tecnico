package com.exchange.rate.api.exchangerate.service;

import com.exchange.rate.api.exchangerate.model.entity.ExchangeRate;
import com.exchange.rate.api.exchangerate.model.request.ExchangeRateRequest;
import com.exchange.rate.api.exchangerate.model.request.ExchangeRateSearchRequest;
import com.exchange.rate.api.exchangerate.model.request.UpdateExchangeRateRequest;
import com.exchange.rate.api.exchangerate.model.response.ExchangeRateResponse;
import com.exchange.rate.api.exchangerate.model.response.ExchangeRateSearchResponse;
import rx.Completable;
import rx.Single;

import java.util.List;

public interface ExchangeRateService {

    Single<ExchangeRateSearchResponse> calculate(ExchangeRateSearchRequest request);
    Single<ExchangeRateResponse> save(ExchangeRateRequest request);

    Completable updateExchangeRate(UpdateExchangeRateRequest exchangeRateRequest);

}


