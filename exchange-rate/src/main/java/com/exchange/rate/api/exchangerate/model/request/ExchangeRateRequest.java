package com.exchange.rate.api.exchangerate.model.request;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ExchangeRateRequest {

    @NonNull
    private BigDecimal exchangeRateValue; //2.3, 3,4
    @NonNull
    private String originCurrency;
    @NonNull
    private String destinationCurrency;


}
