package com.exchange.rate.api.exchangerate.model.request;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ExchangeRateSearchRequest {

    @NonNull
    private BigDecimal amount;
    @NonNull
    private String originCurrency;
    @NonNull
    private String destinationCurrency;
    @NonNull
    private String date;

}
