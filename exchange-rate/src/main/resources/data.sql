INSERT INTO EXCHANGE_RATE (code, origin_currency, destination_currency, exchange_rate_value) VALUES
  ('USDPEN27082021','USD','PEN','3.90'),
  ('USDPEN26082021','USD','PEN','4.20'),
  ('USDPEN25082021','USD','PEN','3.20'),
  ('USDBRL27082021','USD','BRL','4.20'),
  ('EURUSD26082021','EUR','USD','1.17');

