package com.exchange.rate.api.exchangerate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class TestApp {

	private List<String> lista;
	private List<String> numeros;

	public TestApp(){
		lista = new ArrayList<>();
		lista.add("Test");
		lista.add("Code");
		lista.add("Giancarlo");
		lista.add("TestCode");
		
		numeros = new ArrayList<>();
		numeros.add("1");
		numeros.add("2");
	}
	
	public void filtrar(){
		lista.stream().filter(x -> x.startsWith("M")).forEach(System.out::println);
	}
	
	public void ordenar(){
		//lista.stream().sorted().forEach(System.out::println);
		lista.stream().sorted((x,y)-> y.compareTo(x)).forEach(System.out::println);
	}
	
	public void transformar(){
		//lista.stream().map(String::toUpperCase).forEach(System.out::println);
		/*for(String x : numeros){
			int num = Integer.parseInt(x) + 1;
			System.out.println(num);
		}*/
		numeros.stream().map(x -> Integer.parseInt(x) + 1).forEach(System.out::println);
	}
	
	public void limitar(){
		lista.stream().limit(2).forEach(System.out::println);
	}
	
	public void contar(){
		long x = lista.stream().count();
		System.out.println(x);
	}

	/*public void squard() {
		Integer[] numbers = {1, 2, 3, 4, 5};
		Observable.fromArray(numbers)
				.map(n->n*n)
				.filter(squared -> squared<10)
				.forEach(r->log.info(r.toString()));
	}*/

	public void expresionesLambdaTest() {
		Integer[] numbers = {1, 2, 3, 4, 5};
		Stream<Integer> stream = Arrays.stream(numbers);
		stream.map(n -> n*n)
				.filter(squared -> squared < 10).forEach(System.out::println);;
	}

	public void expresionesLambdaTestx() {
		Integer[] numbers = {1, 2, 3, 4, 5};
		Stream<Integer> stream = Arrays.stream(numbers)
				.filter(p -> p %2 == 0)
				.sorted((x,y)-> y.compareTo(x));
	}

	public void expresionesLambdaTest2() {
		Integer[] numbers = {1, 2, 3, 4, 5};
		List<Integer> lista = Arrays.asList(numbers);
		lista.stream()
				.filter(p -> p %2 == 0)
				.sorted((x,y)-> y.compareTo(x)).forEach(System.out::println);
	}
		
	public static void main(String[] args) {
		TestApp app = new TestApp();
		app.expresionesLambdaTest2();
		//app.filtrar();
		//app.ordenar();
		//app.transformar();
		//app.limitar();
		///app.contar();
	}
}
