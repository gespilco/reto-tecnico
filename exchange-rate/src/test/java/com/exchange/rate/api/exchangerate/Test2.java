package com.exchange.rate.api.exchangerate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Test2 {

	public void expresionesLambdaTest2() {
		Integer[] numbers = {1, 2, 3, 4, 5};
		List<Integer> lista = Arrays.asList(numbers);
		lista.stream()
				.filter(p -> p %2 == 0)
				.sorted((x,y)-> y.compareTo(x)).forEach(System.out::println);
	}
		
	public static void main(String[] args) {
		Test2 app = new Test2();
		app.expresionesLambdaTest2();

	}
}
