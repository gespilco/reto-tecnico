package com.exchange.rate.api.exchangerate;

import java.util.Optional;

public class OptionalPresentExample {

	public static void main(String[] args) {
		Optional<String> ofNullable = Optional.ofNullable(null);//"JAVA8" null
		System.out.println(ofNullable.isPresent() ? ofNullable.get() : Optional.empty());
		ofNullable.ifPresent(s-> System.out.println(s));
	}

}
